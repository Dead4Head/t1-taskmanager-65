package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.TaskWebDto;

import java.util.List;

public interface TaskDtoRepository extends AbstractDtoRepository<TaskWebDto> {

    @NotNull
    List<TaskWebDto> findAllByProjectId(@NotNull final String projectId);

    void deleteByProjectId(final @NotNull String projectId);

}
