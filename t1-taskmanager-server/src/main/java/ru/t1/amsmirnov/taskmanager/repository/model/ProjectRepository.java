package ru.t1.amsmirnov.taskmanager.repository.model;

import ru.t1.amsmirnov.taskmanager.model.Project;

public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

}
